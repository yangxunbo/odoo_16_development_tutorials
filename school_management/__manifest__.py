# -*- coding: utf-8 -*-

{
    'name': 'School Management',
    'version': '16.0.1.0.0',
    'summary': 'School Management Software',
    'description': 'Treating Schools',
    'category': 'School',
    'author': 'My Company',
    'website': 'http://www.oscg.cn',
    'license': 'LGPL-3',
    'depends': ['base'],
    'data': [
        "security/ir.model.access.csv",
        "views/school_view.xml",
        "views/student_view.xml",
        "views/hobby_view.xml"

    ],
    'demo': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
