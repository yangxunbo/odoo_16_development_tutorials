from odoo import fields, models, api


class SchoolProfile(models.Model):
    _name = 'school.profile'
    _rec_name = 'name'
    _description = ''

    name = fields.Char(string="School Name",
                       required=False, copy=False,
                       help="This is School Name")
    email = fields.Char(string="Email", required=False, copy=False)
    phone = fields.Char(string="Phone", required=False, copy=False)
    is_virtual_class = fields.Boolean(string="Virtual Class Support?",
                                      help="This Virtual Class Field.")
    school_rank = fields.Integer(string="Rank")
    result = fields.Float(string="Result", digits=(16, 2))
    address = fields.Text(string="Address")
    estalish_date = fields.Date(string="Estalish Date")
    open_date = fields.Datetime(string="Open Date")
    school_type = fields.Selection([('public', 'Public School'),
                                    ('private', 'Private School')], string='School Type')

    documents = fields.Binary(string="Documents")
    documents_name = fields.Char(string="File Name")
    school_image = fields.Image(string="Upload School Image", max_width=100, max_height=100)
    school_description = fields.Html(string="Description")
    auto_rank = fields.Integer(compute="_auto_rank_populate", string="Auto Rank", store=True)
    currency_id = fields.Many2one("res.currency", string="Currency")



    def name_get(self):
        school_list = []
        for school in self:
            name = school.name
            if school.school_type:
                name += " - {}".format(school.school_type)
            school_list.append((school.id, name))
        return school_list

    @api.depends("school_type")
    def _auto_rank_populate(self):
        for rec in self:
            if rec.school_type == "private":
                rec.auto_rank = 50
            elif rec.school_type == "public":
                rec.auto_rank = 100
            else:
                rec.auto_rank = 0

    @api.model
    def name_create(self, name):

        email = "abc123@123.com"
        school_type = "public"
        rec = self.create({'name': name, 'email': email, 'school_type': school_type})

        return rec.name_get()[0]


