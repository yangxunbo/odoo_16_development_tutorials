# -*- coding: utf-8 -*-
import datetime
import random

from lxml import etree
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class StudentAddress(models.Model):
    _name = 'school.student.address'
    _rec_name = 'street'

    street = fields.Char("Street")
    street_one = fields.Char("Street One")
    city = fields.Char("City")
    state = fields.Char("state")
    country = fields.Char("Country")
    zip_code = fields.Char("Zip Code")


class SchoolStudent(models.Model):
    _name = 'school.student'
    _inherit = 'school.student.address'
    _description = 'School Student Model'

    name = fields.Char()
    school_id = fields.Many2one(comodel_name="school.profile", string="School Name", required=False, )
    hobby_ids = fields.Many2many(comodel_name="school.hobby", relation="school_hooby_rel", column1="student_id",
                                 column2="hobby_id", string="Hobby List", )
    is_virtual_school = fields.Boolean(string="Virtual School", related="school_id.is_virtual_class", store=True)
    school_address = fields.Text(string="School Address", related="school_id.address", store=True)

    currency_id = fields.Many2one("res.currency", string="Currency")
    student_fees = fields.Monetary(string="Student Fees")
    total_fees = fields.Float(string="Total Fees")
    ref_id = fields.Reference(string="Reference",
                              selection=[('school.profile', 'School'), ('account.move', 'Invoice'), ], required=False, )
    active = fields.Boolean(string="Active",default=True)
    bdate = fields.Date(string="Date Of Birth")

    def custom_button_method(self):
        print('hello this is custom_button!')
        self.custom_new_method(random.randint(1, 1000))

    def custom_new_method(self, total_fess):
        for rec in self:
            rec.total_fees = total_fess

    @api.model
    def get_view(self, view_id=None, view_type='form', **options):
        res = super(SchoolStudent, self).get_view(view_id, view_type, **options)
        if view_type == 'form':
            doc = etree.XML(res['arch'])
            address_field = doc.xpath("//field[@name='school_address']")
            if address_field:
                address_field[0].set("string", "School Address For GetView Method")
                address_field[0].set("nolabel", "0")

            res['arch'] = etree.tostring(doc, encoding='unicode')

        if view_type == 'tree':
            doc = etree.XML(res['arch'])
            school_field = doc.xpath("//field[@name='school_id']")

            if school_field:
                school_field[0].addnext(etree.Element('field', {'string': 'Total Fees',
                                                                'name': 'total_fees'}))

            res['arch'] = etree.tostring(doc, encoding='unicode')

        return res

    @api.model
    def default_get(self, fields_list):
        rtn = super(SchoolStudent, self).default_get(fields_list)
        rtn['active'] = True
        return rtn

    @api.model
    def create(self, vals_list):

        vals_list["active"] = True
        rtn = super(SchoolStudent, self).create(vals_list)
        return rtn

    def write(self, vals):

        rtn = super(SchoolStudent, self).write(vals)
        return rtn

    def copy(self, default={}):

        default["name"] = self.name + "(copy)"
        rtn = super(SchoolStudent, self).copy(default)

        return rtn

    def unlink(self):
        for rec in self:
            if rec.total_fees > 0:
                raise UserError(_("not in %s del" % rec.name))
        rtn = super(SchoolStudent, self).unlink()

        return rtn


class SchoolProfileInherit(models.Model):
    _inherit = "school.profile"

    student_ids = fields.One2many(comodel_name="school.student", inverse_name="school_id", string="All Student",
                                  required=False, )
    school_number = fields.Char("School Code")

    @api.model
    def name_search(self, name='', args=[], operator='ilike', limit=100):
        print('name--->', name)
        print('args----->', args)
        print('operator----->', operator)
        if name:
            res = self.search(
                ['|', '|', ('name', operator, name), ('email', operator, name), ('school_number', operator, name)])
            print('res1----->', res)
            print('res2----->', res.name_get())
            return res.name_get()
        print('res3---->',
              super(SchoolProfileInherit, self).name_search(name=name, args=args, operator=operator, limit=limit))
        return super(SchoolProfileInherit, self).name_search(name=name, args=args, operator=operator, limit=limit)

    # @api.model
    # def _name_search(self, name='', args=[], operator='ilike', limit=100, name_get_uid=None):
    #     domain = []
    #     if name:
    #         domain = ['|', '|', ('name', operator, name), ('email', operator, name), ('school_number', operator, name)]
    #     print('valu1----->',self.search(domain + args, limit=limit))
    #     print('valu2----->',self.search(domain + args, limit=limit).name_get())
    #     school_ids = self._search(domain+args,limit=limit,access_rights_uid=name_get_uid)
    #     print('valu3----->',school_ids)
    #     return school_ids

    # @api.model
    # def create(self, vals_list):
    #     rtn = super(SchoolProfileInherit, self).create(vals_list)
    #     if not rtn.student_ids:
    #         raise UserError(_("Student not is None! "))
    #     return rtn


class SchoolHobbies(models.Model):
    _name = "school.hobby"

    name = fields.Char(string="Hobby")


class SchoolStudentExtnd(models.Model):
    _inherit = 'school.student'

    parnet_name = fields.Char(string="Parnet Name", required=False, )


class StudentCar(models.Model):
    _name = 'school.car'

    name = fields.Char("Car Name")
    pirce = fields.Float("Pirce")


class CarEngine(models.Model):
    _name = "school.car.engine"
    _inherits = {"school.car": "car_id"}

    name = fields.Char("Car Engine Name")
    car_id = fields.Many2one("school.car", string="Car")
