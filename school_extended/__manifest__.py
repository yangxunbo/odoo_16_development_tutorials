# -*- coding: utf-8 -*-

{
    'name': 'School Extend',
    'version': '16.0.1.0.0',
    'summary': 'School Management Software Extend',
    'description': 'Treating Schools',
    'category': 'School',
    'author': 'My Company',
    'website': 'http://www.oscg.cn',
    'license': 'LGPL-3',
    'depends': ['school_management'],
    'data': [
        'views/student_view.xml',
    ],
    'demo': [

    ],
    'installable': True,
    'application': False,
    'auto_install': False
}
