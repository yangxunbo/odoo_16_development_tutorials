{
    'name': 'Product Extension',
    'version': '16.0.1',
    'author': 'Joey',
    'website': 'http://www.oscg.cn',
    'depends': ['product'],
    'data': [
        'views/product.xml',
    ],
    'installable': True,
    'auto_install': False
}
