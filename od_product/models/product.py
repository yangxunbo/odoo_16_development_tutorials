from odoo import fields, models, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    _description = 'Description'

    processor = fields.Char(string="Computer Processor")
